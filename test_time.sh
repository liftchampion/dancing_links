#!/bin/bash

start=$(date +%s)

ops=$(10)

for (( i=1; i <= ${ops}; i++ ))
do
	./generator test 10
	./fillit test > /dev/null

done

end=$(date +%s)

runtime=$(python -c "print((${end} - ${start})")

echo "Runtime was $runtime"